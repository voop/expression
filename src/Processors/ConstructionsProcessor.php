<?php namespace Voop\Expression\Processors;

use Voop\Money\CurrencyHelper;
use \Voop\Expression\Constructions\ConstructionsInterface;


/**
 * --- Описание ---
 * Класс-процессор конструкций:
 *   - поиск конструкций в тексте и проверка валидности синтаксиса.
 *   - обработка выражения и замена полученного результата на подсчитанный результат
 *
 * --- Как работает ---
 * 1. Анализ кода
 *   - Переденная в Процессор строка проверяется по порядку на наличие в ней конструкций
 *   - Каждая конструкция проверяется на наличие своим обработчиком.
 *   - Каждый обработчик ищет свои конструкции в строке
 *   - Тело найденной конструкции заменяется md5-хэшом
 *   - Обработчик просматривает строку до тех пор, пока не найдет в ней все свои конструкции и не заменит их на md5-хэши
 *   - Наденное выражение должно
 *       -- сначала быть проверено синтаксически (рассчитывать сразу НЕ надо) и...
 *       -- готовый проверенный формат складывается в массив
 *       -- ключ массива - это тот самый md5-хэш
 *   - Поскольку Процессор хранит обрабатываемую строку он ПРОТАСКИВАЕТСЯ по ВСЕЙ цепочке при разборе строки - ЭЛЕМЕНТ ЗА ЭЛЕМЕНТОМ в КАЖДЫЙ класс.
 * 2. Рассчет выражения
 *   - Первоначально конструкция проверяется синтакчически, а обработанный синтаксически готовый к исполнению код кладется в МАССИВ обработчика
 *   - затем по вызову происходит рассчет выражения
 *   - md5-хэш в строке заменяется на рассчитанное выражение
 *
 * Class ExpressionProcessor
 *
 * @package Voop\Expression
 */
class ConstructionsProcessor
{

    /**
     * @var string
     */
    private $basedString;

    /**
     * @var string
     */
    private $string;


    /**
     * @var CurrencyHelper
     */
    private $currencyHelper;

    /**
     * @var array
     */
    private $handlers = [];


    /**
     * Конструкции доступные для обработки, которые будем искать и обрабатывать
     */
    const CONSTRUCTIONS = [
        \Voop\Expression\Constructions\Expression\Exprfunc::class, // 'ВЫРАЖЕНИЕ',
        \Voop\Expression\Constructions\Expression\Moneyfunc::class // 'ДЕНЬГИ',
    ];


    /**
     * @param string         $string
     * @param CurrencyHelper $currencyHelper
     * @return $this
     */
    public function __construct(string $string, CurrencyHelper $currencyHelper)
    {
        $this->currencyHelper = $currencyHelper;
        // первоначальный вариант строки
        $this->basedString    = $string;
        // строка которая будет меняться от итерации к итерации
        $this->string         = $string;
        return $this;
    }

    /**
     * Проверить валидность выражений
     *
     * @return $this
     * @throws \Exception
     */
    public function run() :ConstructionsProcessor
    {
        // прогоняем всвозможные обработчики выражений и обрабатываем найденное
        foreach (self::CONSTRUCTIONS as $handleClass) {
            $handler = new $handleClass($this->currencyHelper);
            if (false == ($handler instanceof ConstructionsInterface)) {
                throw new \Exception('Must be instanceof ConstructionsInterface');
            }
            /** @var ConstructionsInterface $handler */
            // Ищем вхождения искомой Конструкции в строке
            $handler->run($this);
            $this->handlers[] = $handler;
        }
        return $this;
    }


    /**
     * Просчитать проверенное в строке и сбилдить итоговуй строку
     *
     * @param array $params
     * @return $this
     */
    public function build(array $params)
    {
        foreach ($this->handlers as $handler) {
            /** @var ConstructionsInterface $handler */
            $handler->build($this, $params);
        }
        return $this;
    }


    /**
     * Получить текущее состояние обрабатываемой строки
     *
     * @return mixed
     */
    public function getString() :string
    {
        return $this->string;
    }


    /**
     * Установить обрабтанную строку обработанную на предыдущей итерации какойто Конструкцией
     *
     * @param string $string
     */
    public function setPreparedString(string $string)
    {
        $this->string = $string;
    }
}
