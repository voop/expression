<?php namespace Voop\Expression\Processors;

use Voop\Expression\Elements\Arithmetic;
use Voop\Expression\Elements\Brackets;
use Voop\Expression\Elements\Bracketsend;
use Voop\Expression\Elements\Numeric;
use Voop\Expression\Elements\Parameter;
use Voop\Expression\Elements\ElementsInterface;

/**
 * --- Описание ---
 * Класс-процессор:
 *   - поиск конструкций в тексте и проверка валидности синтаксиса.
 *   - обработка выражения и замена полученного результата на подсчитанный результат

 * --- Как работает ---
 * Переденная в Процессор строка проверяется по порядку на наличие в ней конструкций
 *   - Каждая конструкция проверяется на наличие своим обработчиком.
 *   - Каждый обработчик ищет свои конструкции
 *   - тело найденной конструкции заменяется md5-хэшом
 *   - Обработчик просматривает строку до тех пор, пока не найдет в ней все свои конструкции и не заменит их на md5-хэш
 *   - Наденное выражение должно сначала быть проверено синтаксически (рассчитывать НЕ надо) и готвый проверенный формат складывается в массив
 *     ключ массива - это тот самый md5-хэш
 * Анализ кода
 *   - Первоначально конструкция проверяется синтакчически, а обработанный синтаксически готовый к исполнению код кладется в МАССИВ обработчика
 *   - затем по вызову происходит рассчет выражения
 *   - md5-хэш в строке заменяется на рассчитанное выражение
 *
 * Class ExpressionProcessor
 *
 * @package Voop\Expression
 */
class ExpressionProcessor
{

    /**
     * @var string
     */
    private $basedString;

    /**
     * @var string
     */
    private $string;

    /**
     * @var array
     */
    private $findLog = [];


    /**
     * @var array
     */
    private $handlers = [];


    /** */
    const HANDLER_BRACKETS     = 'brackets';
    /** */
    const HANDLER_BRACKETS_END = 'brackets_end';
    /** */
    const HANDLER_PARAMETER    = 'parameter';
    /** */
    const HANDLER_NUMERIC      = 'numeric';
    /** */
    const HANDLER_ARITHMETIC   = 'arithmetic';


    /**
     * Карта последовательности - что за чем может идти
     */
    const HANDLERS_MAP = [
        // что может быть в самом начале
        'start'                  => [
            self::HANDLER_BRACKETS,
            self::HANDLER_PARAMETER,
            self::HANDLER_NUMERIC
        ],
        // что у нас может быть после открывающих скобок "("
        self::HANDLER_BRACKETS  => [
            self::HANDLER_ARITHMETIC,
            self::HANDLER_BRACKETS,
            self::HANDLER_PARAMETER,
            self::HANDLER_BRACKETS_END
        ],
        // что может быть после %ПАРАМЕТРА%
        self::HANDLER_PARAMETER  => [
            self::HANDLER_ARITHMETIC,
            self::HANDLER_BRACKETS_END
        ],
        // что может быть после числа
        self::HANDLER_NUMERIC    => [
            self::HANDLER_ARITHMETIC,
            self::HANDLER_BRACKETS_END

        ],
        // что может быть после арифметического оператора
        self::HANDLER_ARITHMETIC => [
            self::HANDLER_BRACKETS,
            self::HANDLER_PARAMETER,
            self::HANDLER_NUMERIC
        ],
        // что может быть после закрывающей скобки
        self::HANDLER_BRACKETS_END => [
            self::HANDLER_BRACKETS_END,
            self::HANDLER_ARITHMETIC
        ]
    ];


    /**
     * Методы-обработчики
     * ВСЕГДА проверять арифметические знаки self::HANDLER_ARITHMETIC - ПЕРЕД перед числами self::HANDLER_NUMERIC
     * из-за отрицательных чисел: "-2.45" это отрицательное число, а "- 2.45"(с пробелом) это вычитание
     *
     * @var array
     */
    const HANDLERS = [
        self::HANDLER_BRACKETS_END => Bracketsend::class,
        self::HANDLER_BRACKETS     => Brackets::class,
        self::HANDLER_PARAMETER    => Parameter::class,
        self::HANDLER_NUMERIC      => Numeric::class, // ПЕРЕД арифметикой: из-за отрицательных чисел: -1
        self::HANDLER_ARITHMETIC   => Arithmetic::class,
    ];


    /**
     * @param string $string
     */
    public function __construct(string $string)
    {
        $this->string = $string;
        $this->basedString = $string;
        $this->_initHandlerscollection();
    }


    /**
     * Инит коллекции обработчиков
     *
     * @throws \Exception
     */
    private function _initHandlerscollection()
    {
        foreach (self::HANDLERS as $handleKey => $handleClass) {
            $obj = new $handleClass;
            if (false == ($obj instanceof ElementsInterface)) {
                throw new \Exception('Must be instanceof ElementsInterface');
            }

            $this->handlers[$handleKey] = $obj;
        }
    }


    /**
     * Обработка переданного выражения в скобках например: (1+2*<p>%ПАРАМЕТР%</p>)
     *
     * @return ExpressionProcessor|false
     */
    public function run()
    {
        // ищем открывающие скобки и валидное выражение внутри
        $brackets = new Brackets();
        return $brackets->check($this);
    }


    /**
     * Убирает из строки найденое (для следующей итерации в проверке), сохраняет найденое в лог
     *
     * @param ElementsInterface $match
     */
    public function prepareFounded(ElementsInterface $match)
    {
        $this->findLog[] = $match->getCleared();
        $this->string = mb_substr($this->string, mb_strlen($match->getFounded()));
    }

    /**
     * Получить обработанную строку
     *
     * @param bool $glue
     * @return string
     */
    public function getCheckedString(bool $glue = null): string
    {
        return implode($glue ? ' ' : '', $this->findLog);
    }


    /**
     * Получить текущее состояние обрабатываемой строки
     *
     * @return mixed
     */
    public function getString() :string
    {
        return $this->string;
    }


    /**
     * Получить первоначальную строку (с которой начинался просчет)
     *
     * @return mixed
     */
    public function getBasedString() :string
    {
        return $this->basedString;
    }


    /**
     * Лог найденных элементов
     *
     * @return array
     */
    public function getFindLog()
    {
        return $this->findLog;
    }


    /**
     * Возвращает список инициализированных обработчиков (для просчета вложенных выражений)
     *
     * @return array
     */
    public function getHandlers()
    {
        return $this->handlers;
    }


    /**
     * Проверяет может-ли следовать обработчик элемента $followKey за элементом $prevKey - карта следования элементов
     *
     * @param string $prevKey
     * @param string $followKey
     * @return bool
     */
    public function isInHandleMap(string $prevKey, string $followKey) :bool
    {
        return in_array($followKey, self::HANDLERS_MAP[$prevKey]);
    }
}
