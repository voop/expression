<?php namespace Voop\Expression\Elements;

use Voop\Expression\Processors\ExpressionProcessor;

/**
 * Class Arithmetic
 *
 * @see \Voop\Expression\Test\ElementsTest
 * @package Voop\Expression
 */
class Arithmetic extends ElementsBase implements ElementsInterface
{
    /**
     * Арифметический знак: + - / *
     *
     * @param ExpressionProcessor $processor
     * @return array|false
     */
    public function check(ExpressionProcessor $processor)
    {
        return $this->process("[+-/*]", $processor);
    }
}
