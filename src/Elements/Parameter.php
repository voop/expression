<?php namespace Voop\Expression\Elements;

use Voop\Expression\Processors\ExpressionProcessor;

/**
 * Class Parameter
 *
 * @see \Voop\Expression\Test\ElementsTest
 * @package Voop\Expression
 */
class Parameter extends ElementsBase implements ElementsInterface
{
    /** */
    const PARAM_BOUND = '%';

    /**
     * %ПЕРЕМЕННАЯ%
     *
     * @param ExpressionProcessor $processor
     * @return array|false
     */
    public function check(ExpressionProcessor $processor)
    {
        $b = self::PARAM_BOUND;
        return $this->process("{$b}[^\s{$b}]+{$b}", $processor);
    }
}
