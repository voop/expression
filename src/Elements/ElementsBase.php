<?php namespace Voop\Expression\Elements;

use Voop\Expression\Processors\ExpressionProcessor;

/**
 * Class ElementsBase
 *
 * @package Voop\Expression
 */
abstract class ElementsBase implements ElementsInterface
{
    /**
     * @var string
     */
    protected $founded = '';


    /**
     * @var string
     */
    protected $cleared = '';


    /**
     *
     */
    public function setDefault()
    {
        $this->founded = '';
        $this->cleared = '';
    }


    /**
     * Очистить найденное от тэгов, мусора переносов, пробелов
     * Сейчас реализовано через регулярное выражение при поиске
     *
     * @param string $string
     */
    protected function ctearFounded(string $string)
    {
        $this->cleared = trim($string);
    }


    /**
     * Вернуть всю найденную строку - с тэгами пробелами и т.д.
     *
     * @return string
     */
    public function getFounded() :string
    {
        return $this->founded;
    }


    /**
     * Вернуть только сам найденный эемент - очищенный от тэгов, мусора пробелов и т.д.
     *
     * @return string
     */
    public function getCleared() :string
    {
        return $this->cleared;
    }


    /**
     * Ищет совпадение в строке по регулярке, находит и возвращает наденное
     *
     * @param string              $regexp
     * @param ExpressionProcessor $processor
     * @return array|bool
     */
    protected function process(string $regexp, ExpressionProcessor $processor) :bool
    {
        $this->setDefault();
        $matches = [];
        $regexp = "~^(?:(?:<[^>]+?>)|(?:&[^;]+;)|\s)*({$regexp})~ui";
        if (preg_match($regexp, $processor->getString(), $matches)) {
            $this->founded = $matches[0];
            $this->ctearFounded($matches[1]);
            // Если что-то найдено - фиксируем это в объекте ExpressionProcessor
            $processor->prepareFounded($this);
            return true;
        }
        return false;
    }
}
