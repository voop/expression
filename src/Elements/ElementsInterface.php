<?php namespace Voop\Expression\Elements;

use Voop\Expression\Processors\ExpressionProcessor;

/**
 * Interface ElementsInterface
 *
 * @package Voop\Expression
 */
interface ElementsInterface
{
    /**
     * @param \Voop\Expression\Processors\ExpressionProcessor $processor
     * @return mixed
     */
    public function check(ExpressionProcessor $processor);


    /**
     * @return string
     */
    public function getCleared() :string;


    /**
     * @return string
     */
    public function getFounded() :string;
}
