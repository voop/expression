<?php namespace Voop\Expression\Elements;

use Voop\Expression\Processors\ExpressionProcessor;

/**
 * Class Numeric
 *
 * @see \Voop\Expression\Test\ElementsTest
 * @package Voop\Expression
 */
class Numeric extends ElementsBase implements ElementsInterface
{
    /**
     * int, float в том числе и отрицательные
     * 2 или -2 или 2.234 или 2,234 или -2.234 или -2,234 ---> нельзя "- 2.234" (минус с пробелом - это уже вычитание)
     *
     * @param ExpressionProcessor $processor
     * @return array|false
     * @throws \Exception
     */
    public function check(ExpressionProcessor $processor)
    {
        $log = $processor->getFindLog();
        $prevElm = array_pop($log);
        $result = $this->process("-?\d+([\.,]\d+)*", $processor);

        // предыдущий элемент - это деление. Текущий элемент - это 0.
        // TODO - совсем не его зона ответственности - надо переместить в класс-Resolver-todo
        if ($prevElm == '/' && !(int)$this->getCleared()) {
            throw new \Exception("Деление на 0! \"{$this->getFounded()}\": " . mb_substr($processor->getCheckedString(), 0, 30));
        }

        return $result;
    }
}
