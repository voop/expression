<?php namespace Voop\Expression\Elements;

use Voop\Expression\Processors\ExpressionProcessor;

/**
 * Class Bracketsend
 *
 * @see \Voop\Expression\Test\ElementsTest
 * @package Voop\Expression
 */
class Bracketsend extends ElementsBase implements ElementsInterface
{
    /** */
    const CLOSE_BRACE = ')';

    /**
     * Закрывающий символ (выражения в скобках)
     *
     * @param ExpressionProcessor $processor
     * @return array|false
     * @throws \Exception
     */
    public function check(ExpressionProcessor $processor)
    {
        return $this->process('\)', $processor);
    }
}
