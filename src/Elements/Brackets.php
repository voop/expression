<?php namespace Voop\Expression\Elements;

use Voop\Expression\Processors\ExpressionProcessor;

/**
 * Class Brackets
 *
 * @see \Voop\Expression\Test\ElementsTest
 * @package Voop\Expression
 */
class Brackets extends ElementsBase implements ElementsInterface
{
    /** */
    const OPEN_BRACE = '(';


    /**
     * Обработчик выражения в скобках: (1 + 2 * %ПАРАМЕТР% / (10 * 5))
     *
     * @param ExpressionProcessor $processor
     * @return bool
     */
    public function check(ExpressionProcessor $processor) :bool
    {
        // В начале должна быть всегда открывающая скобка
        if (!$find = $this->process('\(', $processor)) {
            // ничего не найдено
            return false;
        }

        // проверяем тело скобок
        return $this->_checkBody($processor);
    }


    /**
     * Обработка тела выражения до нахождения ЗАКРЫВАЮЩЕЙ СКОБКИ: "1 + 2 * %ПАРАМЕТР% / (10 * 5))"
     *
     * @param ExpressionProcessor $processor
     * @return bool
     * @throws \Exception
     */
    private function _checkBody(ExpressionProcessor $processor) :bool
    {
        // Обработчики
        $prevHandler = 'start';
        $prevFound   = null;
        do {
            $method  = null;
            $success = false;

            // Берем коллекцию обработчиков и пытаемся перебором нати следующий элеиент в строке
            // То-есть проверяем валидность того, что внутри
            foreach ($processor->getHandlers() as $method => $handler) {

                // Протаскиваем Процессор дальше по объектам и проверяем строку!
                /** @var ElementsInterface $handler */
                $found = $handler->check($processor);

                // Обработчик не был найден
                if ($found === false) {
                    continue;
                }

                // Проверка по карте следования элементов
                if (! $processor->isInHandleMap($prevHandler, $method)) {
                    throw new \Exception("Некорректное следование элемента \"{$handler->getFounded()}\": " . mb_substr($processor->getCheckedString(), 0, 15));
                }

                $prevHandler = $method;
                $success     = true;

                // Нужное нашли и обработали: выходим из цикла обработчиков
                break;
            }

            // Валидные элементы так и не найдены - т.е. далее какойто неясный шлак...
            if (!$success) {
                throw new \Exception("Некорректные данные в строке:" . mb_substr($processor->getString(), 0, 50));
            }

            // ВЫХОДИМ! Последний обработчик нашел конец выражения - всё успешно обработали
            if ($method == ExpressionProcessor::HANDLER_BRACKETS_END) {
                return true;
            }

        } while ($processor->getString());

        throw new \Exception("Не найдена закрывающая скобка выражения" . mb_substr($processor->getCheckedString(), 0, 50));
    }
}
