<?php namespace Voop\Expression\Constructions;

use Voop\Expression\Processors\ConstructionsProcessor;

/**
 * Interface ConstructionsInterface
 *
 * @package Voop\Expression\Constructions
 */
interface ConstructionsInterface
{
    /**
     * Поиска в строке и логирование найденного
     *
     * @param ConstructionsProcessor $processor
     * @return mixed
     */
    public function run(ConstructionsProcessor $processor);


    /**
     * Поиска в строке и логирование найденного
     *
     * @param ConstructionsProcessor $processor
     * @param array                  $params
     * @return mixed
     */
    public function build(ConstructionsProcessor $processor, array $params);
}
