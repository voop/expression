<?php namespace Voop\Expression\Constructions\Expression;

use Voop\Money\Money;

/**
 * Обработчик для: ВЫРАЖЕНИЕ(1+2-3/4) --> вернет в формате "124,30 руб" (валюта подставляется предустановленная)
 *
 * Class Moneyfunc
 *
 * @package Voop\Expression\Constructions\Expression
 */
class Moneyfunc extends ExpressionFuncBase
{
    const KEY = 'ДЕНЬГИ';

    /**
     * @return string
     */
    public function getKey()
    {
        return self::KEY;
    }


    /**
     * @param mixed $val
     * @return mixed
     */
    public function formatForOutput($val)
    {
        // приведение через string - иначе возможны погрешности
        $val = (int)(string)($val * 100);
        // добавить рассчет
        return $this->currencyHelper->buildNumWithCurrency(new Money($val));
    }
}
