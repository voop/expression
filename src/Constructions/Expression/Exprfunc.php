<?php namespace Voop\Expression\Constructions\Expression;

/**
 *  * Обработчик для: ВЫРАЖЕНИЕ(1+2-3/4) --> вернет в формате "124.3" - тоесть не форматируется, а выводится число "как есть"
 * Class Exprfunc
 *
 * @package Voop\Expression\Constructions\Expression
 */
class Exprfunc extends ExpressionFuncBase
{
    /**
     *
     */
    const KEY = 'ВЫРАЖЕНИЕ';


    /**
     * @return string
     */
    public function getKey()
    {
        return self::KEY;
    }


    /**
     * @param mixed $val
     * @return mixed
     */
    public function formatForOutput($val)
    {
        return (string)$val;
    }
}
