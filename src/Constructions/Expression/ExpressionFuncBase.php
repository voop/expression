<?php namespace Voop\Expression\Constructions\Expression;

use FormulaParser\FormulaParser;
use Voop\Money\CurrencyHelper;
use Voop\Expression\Elements\Brackets;
use Voop\Expression\Processors\ExpressionProcessor;
use Voop\Expression\Constructions\ConstructionsInterface;
use Voop\Expression\Processors\ConstructionsProcessor;

/**
 * Базовый класс для всех функций использующих выражения, вычисляемые в скобках  типа "ВЫРАЖЕНИЕ()" "ДЕНЬГИ()"
 *
 * Class ExpressionFuncBase
 *
 * @package Voop\Expression\Constructions\Expression
 */
abstract class ExpressionFuncBase implements ConstructionsInterface, ExpressionConstructionInterface
{

    /**
     * @var CurrencyHelper
     */
    protected $currencyHelper;


    /**
     * Найденные части
     *
     * @var array
     */
    protected $findParts = [];


    /**
     * @param \Voop\Money\CurrencyHelper $currencyHelper
     */
    public function __construct(CurrencyHelper $currencyHelper)
    {
        $this->currencyHelper = $currencyHelper;
    }


    /**
     * --- Директивы для обработки строки. ---
     * - Ищет в строках чтото типа ВЫРАЖЕНИЕ(<p>1</p> + 2 - 5) или ДЕНЬГИ(<p>1</p> + 2 - 5)
     * - Реплейсит найденное выражение на md5()-хэш в строке,
     * - Сохраняет связку md5()-хэш -vs- Найденное выражение
     * - Ищет следующее вхождение
     * - и т.д. пока не прошерстит всю строку
     * - ВЫРАЖЕНИЕ() - обрабатывается СВОИМ дочетрним обработчиком, ДЕНЬГИ() - СВОИМ
     *
     * --- В итоге: ---
     *  - строка: "строка1 ВЫРАЖЕНИЕ(<p>1</p> + 2) строка2 ВЫРАЖЕНИЕ(1 + 4) строка3"
     *  - превратится в: "строка1 5345c3977b8b5784682b5037c135a898 строка2 306c804c66f7a91c21fcdbaa002108d1 строка3"
     *  - а в самом обработчике будет массив для дальнейшего рассчета и реплейса по хэш-ключам:
     *  [
     *    '5345c3977b8b5784682b5037c135a898' => (1+2)
     *    '306c804c66f7a91c21fcdbaa002108d1' => (1+4)
     * ]
     *
     * @param ConstructionsProcessor $constructionProcessor
     * @return ConstructionsInterface
     * @throws \Exception
     */
    public function run(ConstructionsProcessor $constructionProcessor) :ConstructionsInterface
    {
        while (true) {
            // ключ по которому искать типа "ВЫРАЖЕНИЕ"
            $exprKey   = $this->getKey();
            $openBrace = '\\' . Brackets::OPEN_BRACE;
            $regexp    = "/^(.*?){$exprKey}(?:<[^>]+?>)*([{$openBrace}].+)/uis";
            $string    = $constructionProcessor->getString();

            // Ищем чтото типа: ВЫРАЖЕНИЕ(<p>1</p> + 2 - 5)
            if (!preg_match($regexp, $string, $find)) {
                // ВЫХОД! Выражение в обрабатываемой строке НЕ найдено
                break;
            }

            // Далее должно следовать выражение в скобках: (<p>1</p> + 2 - 5) - надо проверить и обработать
            $expressionProcessor = new ExpressionProcessor($find[2]);
            $expressionProcessor->run();
            // Найденная и выверенная строка вида: (1+2-5+%ПАРАМ%)
            $checkedString = $expressionProcessor->getCheckedString();

            // Если не найдны скобки со внутренностями - это ошибка
            if (!$checkedString) {
                throw new \Exception('Неверные данные "%s" после констркции %s', $find[2], $exprKey);
            }

            $md5Key = uniqid(__METHOD__);

            // Связка хэш в тексте для дальнейшего реплейса
            $this->findParts[$md5Key] = $checkedString;

            // Укороченный остаток строки (из нее уже убрали всё найденное)
            $endOfString = $expressionProcessor->getString();

            /**
             * Собираем строку для следующей итерации:
             * - найденное регуляркой начало строки ДО Конструкции
             * - md5-хэш отформатированного результата Конструкции
             * - укороченный остаток строки ПОСЛЕ Конструкции
             */
            $strBuild = sprintf('%s%s%s', $find[1], $md5Key, $endOfString);

            // Пихаем собранную строку для следующей итерации
            $constructionProcessor->setPreparedString($strBuild);
        }

        return $this;
    }


    /**
     * Просчитать всё, что найдено функцией run()
     *
     * @param ConstructionsProcessor $constructionProcessor
     * @param array                  $params
     * @return ConstructionsInterface
     * @throws \Exception
     */
    public function build(ConstructionsProcessor $constructionProcessor, array $params)
    {
        $string = $constructionProcessor->getString();
        foreach ($this->findParts as $key => $construction) {
            // Заменяем %ПАРАМТЕРЫ% на их значения
            $prepStr   = str_replace(array_keys($params), array_values($params), $construction);
            $precision = 2; // 2 знака после запятой
            $parser    = new FormulaParser($prepStr, $precision); // Сторонняя библиотечка для рассчета из строки
            $calc      = $parser->getResult(); // [0 => 'done', 1 => 16.38]
            if ($calc[0] === 'error') {
                throw new \Exception('Ошибка подсчета ' . $construction);
            }
            $calcOut = $this->formatForOutput($calc[1]);
            $string  = str_replace($key, $calcOut, $string);
        }

        $constructionProcessor->setPreparedString($string);
        return $this;
    }
}
