<?php namespace Voop\Expression\Constructions\Expression;

/**
 * Интерфейс для Конструкций типа "ВЫРАЖЕНИЕ()" "ДЕНЬГИ()"
 *
 *
 * Interface ExpressionConstructionInterface
 *
 * @package Voop\Expression\Constructions\Expression
 */
interface ExpressionConstructionInterface
{
    /**
     *
     * @return mixed
     */
    public function getKey();


    /**
     * @param mixed $val
     * @return mixed
     */
    public function formatForOutput($val);
}
