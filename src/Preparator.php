<?php namespace Voop\Expression;

use Voop\Expression\Elements\Parameter;
use Voop\Expression\Processors\ConstructionsProcessor;
use \Voop\Money\CurrencyHelper;

/**
 * Входная точка и буфер между Бизнес-логикой приложения и пакетом
 *
 * Сервис - работает Конструкциями (например с ВЫРАЖЕНИЯМИ(...)) в переданной строке.
 *
 * - Проверяет валидность выражения через self::prepareExpressions()
 * - Билдит значения переданных %ПАРАМЕТРОВ% и просчитывает результат через self::buildExpressions()
 *
 * Class Preparator
 *
 * @see \Voop\Expression\Test\PreparatorTest
 * @package Voop\Expression
 */
class Preparator
{
    /**
     * @var CurrencyHelper
     */
    private $currencyHelper;


    /**
     * @param string $currency
     */
    public function __construct(string $currency = null)
    {
        $this->currencyHelper = new CurrencyHelper($currency);
    }


    /**
     * Просто проверить выражение на валидность следования элементов.
     * Вернет проверенную строку или выкинет exception
     *
     * @param string $string
     * @return bool
     */
    public function prepareExpressions(string $string) :bool
    {
        // Обрабатываем строку - будет Эксепшн или все нормально отработает
        (new ConstructionsProcessor($string, $this->currencyHelper))->run();
        return true;
    }


    /**
     * Заменить %ШАБЛОНЫ% параметров на их значения и ПРОСЧИТАТЬ результат
     *
     * @param string $string
     * @param array  $params
     * @return mixed
     * @throws \Exception
     */
    public function buildExpressions(string $string, array $params) :string
    {
        // Обрабатываем строку
        $processor = (new ConstructionsProcessor($string, $this->currencyHelper))
            ->run()
            ->build($params);
        // Вернуть обработанную строку
        $string = $processor->getString();

        // Заменить всё, что осталось в строке
        $string = str_replace(array_keys($params), array_values($params), $string);

        // Если остались незамененными какие-то %ПАРАМЕТРЫ%
        $b     = Parameter::PARAM_BOUND;
        $found = [];
        if (preg_match("/({$b}[^\s]+{$b})/", $string, $found)) {
            throw new \Exception('Неизвестный парметр ' . $found[1]);
        }

        return $string;
    }
}
