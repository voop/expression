<?php namespace Voop\Expression\Test;

use PHPUnit\Framework\TestCase;
use Voop\Expression\Elements\Arithmetic;
use Voop\Expression\Elements\Bracketsend;
use Voop\Expression\Elements\Numeric;
use Voop\Expression\Elements\Parameter;
use Voop\Expression\Processors\ExpressionProcessor;

/**
 * Class ElementsTest
 *
 * @see \Voop\Expression\Elements\Arithmetic;
 * @see \Voop\Expression\Elements\Bracketsend;
 * @see \Voop\Expression\Elements\Numeric;
 * @see \Voop\Expression\Elements\Parameter;
 * @package Voop\Expression\Test
 */
class ElementsTest extends TestCase
{
    /**
     *
     */
    public function dataProviderForTestArithmetic()
    {
        return [
            ['* 10',        '*'],
            ['/ 10',        '/'],
            ['- 10',        '-'],
            ['-10',         '-'],
            ['- 1 0',       '-'],
            ['-1',          '-'],
            ['+ 10',        '+'],
            ['+',           '+'],
            ['',            false],
            ['%ЧТОТО ЕЩЕ%', false],
        ];
    }

    /**
     * @param string $string
     * @param mixed  $assert
     * @dataProvider  dataProviderForTestArithmetic
     */
    public function testArithmetic($string, $assert)
    {
        $expression = new Arithmetic();
        $expression->check(new ExpressionProcessor($string));
        $this->assertEquals($assert, $expression->getCleared(), $string);
    }


    /**
     *
     */
    public function dataProviderForTestNumeric()
    {
        return [
            ['<p>-1 + 2',           '-1'],
            ['<p>-1</p> + 2',       '-1'],
            [' <p> </p>-1</p> + 2', '-1'],
            ['1 + 2',               '1'],
            ['1.234 + 2',           '1.234'],
            ['-1.234 + 2',          '-1.234'],
            ['1,234 + 2',           '1,234'],
            ['-1,234 + 2',          '-1,234'],
            ['- 1,234 + 2',         false], //это НЕ обработает (т.к. это вычитание)
            ['- 1 234 + 2',         false], //это НЕ обработает (т.к. это вычитание)
            ['-1 234 + 2',          '-1'],
        ];
    }


    /**
     * @param string $string
     * @param mixed  $assert
     * @dataProvider  dataProviderForTestNumeric
     */
    public function testNumeric(string $string, $assert)
    {
        $expression = new Numeric();
        $expression->check(new ExpressionProcessor($string));
        $this->assertEquals($assert, $expression->getCleared(), $string);
    }

    /**
     *
     */
    public function dataProviderForTestParameter()
    {
        return [
            ['%ПАРАМЕТР%',                       '%ПАРАМЕТР%'],
            ['%ПАРАМЕТР_1%',                     '%ПАРАМЕТР_1%'],
            ['%ПАРАМЕТР1% И %ПАРАМЕТР2%',        '%ПАРАМЕТР1%'],
            ['<p>%ПАРАМЕТР1% + %ПАРАМЕТР2%',     '%ПАРАМЕТР1%'],
            ['<p>%ПАРАМЕТР1%</p> + %ПАРАМЕТР2%', '%ПАРАМЕТР1%'],
            [' <p> </p>%ПАРАМЕТР1%</p> + 2',     '%ПАРАМЕТР1%'],
            ['%ПАРАМЕТ',                         false],
            ['ПАРАМЕТ',                          false],
        ];
    }


    /**
     * @param string $string
     * @param mixed  $assert
     * @dataProvider  dataProviderForTestParameter
     */
    public function testParameter(string $string, $assert)
    {
        $expression = new Parameter();
        $expression->check(new ExpressionProcessor($string));
        $this->assertEquals($assert, $expression->getCleared(), $string);
    }

    /**
     *
     */
    public function dataProviderForTestEndwrapper()
    {
        return [
            [') + 1', ')'],
            ['<p>)</p> + 1', ')'],
        ];
    }


    /**
     * @param string $string
     * @param mixed  $assert
     * @dataProvider  dataProviderForTestEndwrapper
     */
    public function testEndwrapper(string $string, $assert)
    {
        $expression = new Bracketsend();
        $expression->check(new ExpressionProcessor($string));
        $this->assertEquals($assert, $expression->getCleared(), $string);
    }
}
