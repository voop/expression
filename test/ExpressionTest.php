<?php namespace Voop\Expression\Test;

use PHPUnit\Framework\TestCase;
use Voop\Expression\Processors\ExpressionProcessor;

/**
 * Class ExpressionTest
 * @see \Voop\Expression\Expression
 * @package Voop\Expression\Test
 */
class ExpressionTest extends TestCase
{
    /**
     *
     */
    public function dataProviderForTestExpression()
    {
        return [
            ['(1)',                                                      '(1)'],
            ['(1.1)',                                                    '(1.1)'],
            ['(1,2)',                                                    '(1,2)'],
            ['(%ПАРАМ%)',                                                '(%ПАРАМ%)'],
            ['(%ПАРАМ1% + %ПАРАМ2%)',                                    '(%ПАРАМ1%+%ПАРАМ2%)'],
            ['(1 + 2)',                                                  '(1+2)'],
            ['(1 + (2 + 3))',                                            '(1+(2+3))'],
            ['(1 + (2.2 + 2,2 + -3 - -1 / -1 * -0) + %ПАРАМ%)',          '(1+(2.2+2,2+-3--1/-1*-0)+%ПАРАМ%)'],
            ['(1 + (2 + 3) + %ПАРАМ% - 2 * 10 * (%ПАРАМ2% + %ПАРАМ3%))', '(1+(2+3)+%ПАРАМ%-2*10*(%ПАРАМ2%+%ПАРАМ3%))'],
            ['(1 + (2 + 3 + (4 + (5 + 6-(7/8)))))',                      '(1+(2+3+(4+(5+6-(7/8)))))'],
            ['(1
                + (2
                + 3)
               )',                                                       '(1+(2+3))'], // Переносы строк
            ['(1 + (2 + 3)',                                             false, "Не найдена закрывающая скобка выражения"],
            ['(1 + (2 + 3) шлак)',                                       false, "Некорректные данные в строке"],
            ['(1 + (2 + + 3))',                                          false, "Некорректное следование элемента"],
            ['(1 / 0)',                                                  false, "Деление на 0"],

            ['(1 / -0)',                                                 false, "Деление на 0"],
            ['(5*(30875.00+2)0.00)',                                     false, "Некорректное следование элемента"], // пропущен оператор перед 0.00
        ];
    }


    /**
     * @param string $string
     * @param mixed  $assert
     * @param mixed  $ecxeptMessage
     * @dataProvider  dataProviderForTestExpression
     */
    public function testExpression(string $string, $assert, $ecxeptMessage = null)
    {
        $processor = new ExpressionProcessor($string);

        if (!$assert) {
            $this->expectException(\Exception::class);
        }

        $processor->run();

        if ($assert) {
            $this->assertEquals($assert, $processor->getCheckedString(), $string);
        } else {
            // выцепляем текст ошибки из начала строки
            $this->expectExceptionMessageRegExp("/^{$ecxeptMessage}.+/u");
        }
    }
}
